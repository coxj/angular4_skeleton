# README #

### How do I get set up? ###

Make sure you have at least Node 6.x or higher (w/ npm 3+) installed!

Visual Studio 2017

Make sure you have .NET Core 1.0+ installed and/or VS2017. VS2017 will automatically install all the neccessary npm & .NET dependencies when you open the project.

Simply push F5 to start debugging !

Visual Studio Code

Note: Make sure you have the C# extension & .NET Core Debugger installed.
The project comes with the configured Launch.json files to let you just push F5 to start the project.

# cd into the directory you cloned the project into
npm install && npm run build:dev && dotnet restore

# or yarn install
If you're running the project from command line with dotnet run make sure you set your environment variables to Development (otherwise things like HMR won't work).

# on Windows:
set ASPNETCORE_ENVIRONMENT=Development
# on Mac/Linux
export ASPNETCORE_ENVIRONMENT=Development